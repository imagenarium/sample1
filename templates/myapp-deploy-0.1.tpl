<@requirement.CONSTRAINT 'myapp' 'true' />

<@requirement.PARAM name='PUBLISHED_PORT' value='9090' description='Внешний порт' />
<@requirement.PARAM name='VERSION' type='tag' description='Версия приложения' />

<@img.TASK 'myapp-${namespace}' 'registry.gitlab.com/imagenarium/sample1/myapp:${PARAMS.VERSION}'>
  <@img.CONSTRAINT 'myapp' 'true' />
  <@img.PORT PARAMS.PUBLISHED_PORT '8080' />
  <@img.CHECK_PORT '8080' />
</@img.TASK>
